'use strict'

//var imagemin = require('gulp-imagemin');
//     var minify = require('gulp-minify');
//var concat = require('gulp-concat');
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    browserSync = require('browser-sync'),
    sourcemaps = require('gulp-sourcemaps'),
    image = require('gulp-image'),
    gulpif = require('gulp-if'),
    del = require('del'),
    uglify = require('gulp-uglify'),
    cssmin = require('gulp-cssmin');

//const isDevelopment = !process.env.NODE_ENV == "develop" || process.env.NODE_ENV == "develop";
const isDevelopment = true;

// gulp.task('test:v', function () {
// 	console.log(isDevelopment)
// });

gulp.task('default',  ['build','watcher', 'browserSync']);

gulp.task('watcher', function() {
    gulp.watch('./dev/*.html', ['html']);
    gulp.watch('./dev/js/*.js', ['js']);
    gulp.watch('./dev/img/*', ['imagemin']);
    gulp.watch('./dev/scss/**/*.scss', ['scss']);
});

gulp.task('scss', function () {
    gulp.src('./dev/scss/*.scss')
        .pipe(gulpif(isDevelopment, sourcemaps.init()))
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(autoprefixer())
        // .pipe(cssmin())
        .pipe(gulpif(isDevelopment,sourcemaps.write()))
        .pipe(gulp.dest('./dist/css'))
        .on('end', browserSync.reload);
});

gulp.task('html', function() {
    gulp.src('./dev/*.html')
        .pipe(gulp.dest('./dist'))
        .on('end', browserSync.reload);
    //.pipe(browserSync.reload({stream: true}));
});

gulp.task('js', function() {
    gulp.src('./dev/js/*.js')
    //.pipe(concat('app.js'))
    //     .pipe(uglify())
        .pipe(gulp.dest('./dist/js'))
        .on('end', browserSync.reload)
    //.pipe(browserSync.reload({stream: true}));
});

gulp.task('imagemin', function() {
    gulp.src('./dev/img/*')
        .pipe(image())
        .pipe(gulp.dest('dist/img'))
        .on('end', browserSync.reload);
});

// gulp.task('imagemin', function() {
//     gulp.src('./dev/img/*')
//         .pipe(imagemin())
//         .pipe(gulp.dest('dist/img'))
//         .pipe(browserSync.reload({stream: true}));
// });

gulp.task('clear', function() {
    del(['./dist/**/*.*',
        '!./dist/css/library/*.css',
        '!./dist/fonts/**/*.*'
    ])
});

gulp.task('build', [
    'scss',
    'html',
    'js',
    'imagemin',
]);


gulp.task('browserSync', function() {
    browserSync({
        server: {
            baseDir: './dist/'
        },
        port: 8080,
        open: true,
        notirfy: false
    });
});


