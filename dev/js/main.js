$(document).ready(function() {

    // slider
    $('.js-main-slider-tem').addClass('animation');
    setInterval( function () {
        initSlider();
    }, 8000);

    // menu
    $('.js-tootle-sub-menu > a').on('mouseenter', function () {
       $(this).closest('.js-tootle-sub-menu').addClass('open');
    });

    $('.js-tootle-sub-menu').on('mouseleave', function () {
        $(this).removeClass('open');
    });

    // tabs
    $('.js-nav-tab').click(function () {
        var index = $(this).data('nav-tads-id');
        var parentElem = $(this).closest('.js-tads-wrap');
        parentElem.find('.js-nav-tab').removeClass('active');
        parentElem.find('.js-nav-content').removeClass('active');
        parentElem.find('.js-nav-tab[data-nav-tads-id='+index+']').addClass('active');
        parentElem.find('.js-nav-content[data-content-tads-id='+index+']').addClass('active');
    });

    //only one number
    $('.js-only-one-number').on('keyup', function (e) {
        var index = $(this).data('index');
            if (index != $('.js-only-one-number').length) {
                $('.js-only-one-number[data-index="'+(index+1)+'"]').focus();
            }
            return false
        // }
    });
    $('.js-only-one-number').on('keypress', function (e) {
       if($(this).val().length == 1) {
           $(this).val(e.key);
           return false
       }
    });

    $('.js-add-another').on('click', function () {
        var inputName = $('.js-box-single-input:last-child input').attr('name');
        var numberName = +inputName.split('_')[1];
        var content = '<div class="form-group js-box-single-input">' +
                        '<input type="text" name="users_'+ (numberName + 1)+'" placeholder="name@mail.com">' +
                      '</div>';
        $('.js-box-all-inputs').append(content);
    })
});

// $(window).scroll( function (e) {
//     if ($(window).scrollTop() > 0 && !$('.js-main-header').hasClass('active')) {
//         $('.js-main-header').addClass('active');
//     } else if ($(window).scrollTop() == 0 && $('.js-main-header').hasClass('active')) {
//         $('.js-main-header').removeClass('active');
//     }
// });

function initSlider() {
    var images = [];
    var bg;
    $('.js-main-slider-tem').each( function (index, item) {
        bg = $(item).css('background-image');
        images.push(bg.replace('url(','').replace(')','').replace(/\"/gi, ""))
    });

    $('.js-main-slider-tem').each( function (index, item) {
        if ($('.js-main-slider-tem').length-1 == index) {
            $(item).css('background-image', 'url("' + images[0] + '")');
        } else {
             $(item).css('background-image', 'url("' + images[index+1] + '")');
        }
    });
}
